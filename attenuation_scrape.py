import requests
import numpy
from bs4 import BeautifulSoup

def scrape_atten_array(url, z_number, max_energy=20, min_energy=0.001):
    """
    Passes element information to XCOM NIST calculator to scrape attenuation
    data. Plain text file is saved in file path with Z # as file name.

    Inputs:
    z_number = Z number of element
    max_energy = Max energy of attenuation data (MeV)
    min_energy = Min energy of attenuation data (MeV)
    file_path = File path to save plain text file of attenuation data
    url = Url of XCOM NIST input form

    Outputs:
    numpy array of attenuation table in cm^2/g in the form of ...
        Photon Energy(MeV)  Coherent  Incoherent  Photo_Abs
        Pair_Prod(Nuclear Field)  Pair_Prod(Electron Field)
        Tot_Atten(w/Coherent)  Tot_Atten(w/out Coherent)
    """
    # Default inputs to XCOM -> Minimum inputs needed to pass to url
    payload = {
        "ZNum":z_number,
        "OutOpt":"PIC",
        "Graph6":"on",
        "WindowXmin":min_energy,
        "WindowXmax":max_energy,
        "NumAdd":1,
        "Output":"on",
        "ResizeFlag":"on"
    }

    req = requests.post(url, data=payload, timeout=5)
    soup = BeautifulSoup(req.text,'html.parser')

    # Find the baige table by its unicode color
    # Extract html code for parsing
    table_rows = soup.find_all('tr', attrs={'bgcolor':'#FFFFCC'})
    atten_data = numpy.zeros((len(table_rows), len(table_rows[0].find_all('td')[1:])))
    i = 0

    for tr in table_rows:
        tds = tr.find_all('td')
        data = " ".join([td.get_text() for td in tds[1:]])
        atten_data[i,:] = numpy.fromstring(data, sep=' ')
        i += 1
    return atten_data

def scrape_atten_txt(url, file_path, z_number, max_energy=20, min_energy=0.001):
    """
    Passes element information to XCOM NIST calculator to scrape attenuation
    data. Plain text file is saved in file path with Z # as file name.

    Inputs:
    z_number = Z number of element
    max_energy = Max energy of attenuation data (MeV)
    min_energy = Min energy of attenuation data (MeV)
    file_path = File path to save plain text file of attenuation data
    url = Url of XCOM NIST input form

    Outputs:
    plain text file of attenuation data in the form of "Z_{z_number}.txt"
    table in cm^2/g in the form of
      Photon Energy(MeV)  Coherent  Incoherent  Photo_Abs
        Pair_Prod(Nuclear Field)  Pair_Prod(Electron Field)
        Tot_Atten(w/Coherent)  Tot_Atten(w/out Coherent)
    """
    # Default inputs to XCOM -> Minimum inputs needed to pass to url
    payload = {
        "ZNum":z_number,
        "OutOpt":"PIC",
        "Graph6":"on",
        "WindowXmin":min_energy,
        "WindowXmax":max_energy,
        "NumAdd":1,
        "Output":"on",
        "ResizeFlag":"on"
    }

    req = requests.post(url, data=payload, timeout=5)
    soup = BeautifulSoup(req.text,'html.parser')

    # Find the baige table by its unicode color
    # Extract html code for parsing
    table_rows = soup.find_all('tr', attrs={'bgcolor':'#FFFFCC'})

    with open(file_path + "/Z_" + str(payload["ZNum"]) + ".txt", 'w') as file:
        for tr in table_rows:
            tds = tr.find_all('td')
            data = " ".join([td.get_text() for td in tds[1:]])
            file.write("{}\n".format(data))
