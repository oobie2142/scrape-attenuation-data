Xcom attenuation data web scraper.

Two functions that can save data to numpy
array or save attenuation data to plain text file. 

XCOM NIST Attenuation form data

#Z number of element you want
'ZNum':31,

#Number of energies per line in output text
'NumAdd':1,

#Outputs attenuation in cm^2/g
'OutOpt':'PIC',

#Option to plot total attenuation -> needed to run
'Graph6':'on',

#Option for standard grid plotting -> needed to run
'Output':'on',
'Resol':'-r72.73x72.73',

#Max and Min energy for data
'WindowXmin':energy,
'WindowXmax':energy*10

Table Data in the form of:

Photon Energy  Coherent  Incoherent  Photo Abs  Pair Prod Nuclear
Pair Prod Electron  Total w/ Coherent  Total w/out Coherent